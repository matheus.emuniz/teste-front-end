### Teste para candidatos à vagas de Front-End

---

O objetivo deste desafio é permitir uma melhor avaliação das suas habilidades como candidato à vaga de Front-end, de vários níveis. Ele deve ser feito 
por você em sua casa.  Gaste o tempo que você quiser, porém normalmente você não deve precisar de mais do que alguns dias.

### Instruções de entrega do desafio

---

Primeiro, faça um fork deste projeto para sua conta no Gitlab (crie uma se você não possuir). Em seguida, implemente o projeto tal 
qual descrito abaixo, em seu próprio fork. Por fim, empurre todas as suas alterações para o seu fork no Gitlab e envie um e-mail para 
felippe.jose@granter.com.br com o assunto Teste vaga Front-End


### Descrição do projeto 

---

Nosso desafio consiste em implementar uma aplicação client-side. Seguindo o arquivo de [história](HISTORY.md) 
realize o desenvolvimento conforme os requisitos e específicação de design detalhadas no Figma.

O projeto vem com o framework Vue 3.0 e a livraria [element-plus](https://element-plus.org/#/en-US/component/layout) pré-instalados
e esses **devem** ser utilizada como base para desenvolver a tela. Além disso você pode instalar qualquer outra 
livraria que achar necessário.

Para popular a tela está disponível um arquivo `mock_data.json` na pasta `src/assets/` 
com o seguinte formato:
```json
{
    "id":"ac90073e-f408-4bba-876c-5a1e5260b3e0",
    "int_total_answers":77,
    "str_name":"feugiat non pretium quis lectus suspendisse potenti in",
    "str_description":"Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.",
    "str_publication_type":null,
    "ts_created_at":"2021-01-26T06:44:30Z",
    "ts_published_at":null,
    "ts_publication_start":null,
    "ts_publication_end":null,
    "lst_tags":[
        "mi",
        "nulla",
        "ac",
        "enim"
    ]
}
```

Os campos desse objeto JSON são mapeados da seguinte:

![cardexemplosofrimento](/imgs/card_exemplo_sofrimento.png)

### Diretrizes:
- Typescript deve ser utilizado
- A forma que o JSON será carregado na aplicação fica a seu critério
- Os botões no layout no design do Card não devem ter funcionalidade
- Uma entrada é considerada `Rascunho` quando o campo `str_publication_type` é igual a `null`
- Utilize os ícones disponíveis na element-plus mesmo que esses não correspondam aos definidos no design
- Para estilização você pode utilizar css, scss ou sass
- Encorajamos o uso do [Vue Compostion API](https://v3.vuejs.org/guide/composition-api-introduction.html)
- vue-router/vuex não são necessários
- O projeto pode ser componentizado a seu critério
- Nenhuma build deve ser gerada o projeto será avaliado em modo de desenvolvimento

---

### Setup
#### Instalação
```
yarn install
```
#### Rodando o projeto em modo de desenvolvimento
```
yarn serve
```
